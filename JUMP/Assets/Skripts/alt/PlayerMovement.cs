﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jump
{
    public class PlayerMovement : MonoBehaviour
    {
	    public CharacterController2D controller;
	    public Animator animator;
	    public float runSpeed = 40f;

	    float horizontalMove = 0f;
	    bool jump = false;
        public bool canMove = true;

        CharacterController2D characterController;

        // Update is called once per frame
        void Update()
	    {
            if (!canMove)
            { animator.SetFloat("Speed",0);
                return;
            }

            horizontalMove = Input.GetAxis("Horizontal") * runSpeed;

		    //animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

		    if (Input.GetButtonDown("Jump"))
		    {
				Debug.Log("Jump");
			    jump = true;
			    //animator.SetBool("IsJumping", true);
				//animator.SetFloat("jumpPressure", characterController.m_JumpForce + characterController.JumpForceMin);
		    }

	    }

	    public void OnLanding()
	    {            
		    //animator.SetBool("IsJumping", false);
			//animator.SetFloat("jumpPressure", 0f);
		}
	    void FixedUpdate()
	    {
            if (!canMove)
            {
                return;
            }
            // Move our character
            controller.Move(horizontalMove * Time.fixedDeltaTime, jump);
		    jump = false;
	    }

      
    }
}